import sys
sys.path.append('../')

import app.fruit_detection
from os import listdir
from os.path import isfile, join

image_path = './sample_images/'

sample_images = [join(image_path, image_name) for image_name in listdir(image_path) if isfile(join(image_path, image_name))]

if __name__ == '__main__':
    for image in sample_images:
        output_data = app.fruit_detection.process_image(image)
        print(output_data, "\n")