import os
import torch
from PIL import Image, ExifTags
import math
import cv2
import numpy as np
import albumentations as A
from albumentations.pytorch import ToTensorV2


def process_image(file_in: str, confidence_threshold: float = 0.9) -> dict:
    """
    Reads the input image, pre-processes it and runs it through the fruit
    prediction model. The model and its parameters are read from a standard
    location. The model classifies the image as 'Compliant' (targetDetected is
    True) (i.e. image likely contains 'Compliant' fruit/vegetable plants), or 'Non-compliant'
    (targetDetected is False). When the probability of the prediction is below
    the specified confidence threshold, the result will always be 'Non-compliant'.

    :param file_in: Path of the image to classify
    :param confidence_threshold: to apply to the prediction
    :return: dictionary (as string) with the entry "results" and containing
        the following:
        - "targetDetected" (boolean): True if the image is classified as
            'Compliant', with a probability >= confidence_threshold
        - "maxConfidence" (float): The probability of the prediction
    """
    # load and preprocess the image
    print(f"preprocessing {file_in}")
    image = prep_image(file_in)
    image_array = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)

    # normalize RGB values using standard parameters
    normalize_transform = A.Compose(
        [
            A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
            ToTensorV2(),
        ]
    )
    norm_image = normalize_transform(image=image_array)["image"]

    # load model
    model_file = os.path.abspath(os.path.join(os.getcwd(), "../app/model/model_fruit_v1.pt"))
    print(f"loading model from file: {model_file}")
    model = torch.load(model_file, map_location=torch.device('cpu'))

    # use model for inference
    print("predicting...")
    model = model.eval()
    with torch.no_grad():
        input_t = norm_image.unsqueeze_(0).to('cpu')
        pred_t = model(input_t)
        probs_t = torch.sigmoid(pred_t)
        if probs_t >= confidence_threshold:
            print(f"result: Image Compliant; prediction={pred_t} probs={probs_t} above threshold ({confidence_threshold})")
            pred = True
        else:
            print(f"result: Image Non-compliant; prediction={pred_t} probs={probs_t} below threshold ({confidence_threshold})")
            pred = False

    tags = ['detect-fruit'] if pred else []

    output_data = {
        "tags": tags,
        "results": {
            "targetDetected": pred,
            "maxConfidence": probs_t.item(),
        }
    }
    return output_data


def prep_image(file_in: str) -> Image:
    # read image
    im = Image.open(file_in)

    # target pixel size
    tpix = 500

    # trying exif orientation stuff
    try:
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                break
        exif = im.getexif()
        if exif[orientation] == 3:
            im = im.rotate(180, expand=True)
        elif exif[orientation] == 6:
            im = im.rotate(270, expand=True)
        elif exif[orientation] == 8:
            im = im.rotate(90, expand=True)
    except (AttributeError, KeyError, IndexError):
        # cases: image don't have exif tags
        pass

    # get image dimensions
    width, height = im.size

    # resize images to target pixel size for smallest side
    if width >= height:
        nheight = tpix
        nwidth = math.ceil(nheight / height * width)
    else:
        nwidth = tpix
        nheight = math.ceil(nwidth / width * height)

    print("resizing " + file_in + " (" + str(nwidth) + "," + str(nheight) + ")")
    im.thumbnail((nwidth, nheight), Image.Resampling.LANCZOS)

    # get image dimensions after resize
    twidth, theight = im.size

    # crop image
    left = (twidth - tpix) / 2
    top = (theight - tpix)
    right = (twidth + tpix) / 2
    bottom = theight

    print("cropping " + file_in + " (" + str(left) + "," + str(top) + "," + str(right) + "," + str(bottom) + ")")
    im = im.crop((left, top, right, bottom))
    return im


# test when running this script as main
if __name__ == '__main__':
    impath = os.path.join(os.getcwd(), "../test/sample_images/compliant (1).jpg")
    print(process_image(impath))
