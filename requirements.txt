numpy==1.23.3
Pillow==9.2.0
opencv-python-headless==4.6.0.66
torch==1.12.1
albumentations==1.3.0
torchvision==0.13.1
